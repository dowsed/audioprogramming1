clc; clear; format longG;

%load original sample file and create plotting variables
[sample, Fs] = audioread("sample.wav");
numSamples = length(sample);
Ts = 1/Fs;
timeSecs = (numSamples-1) / Fs;
timeVec = 0:Ts:timeSecs;

%load sample rate changed file and create plotting variables
[sampleRateChanged, Fs2] = audioread("sampleRateChanged.wav");
Ts2 = 1/Fs2;
timeSecs2 = (numSamples-1) / Fs2;
timeVec2 = 0:Ts2:timeSecs2;

%load sample processed with region of silence
[sampleWithSilence] = audioread("sampleWithSilence.wav");

%limit checks based on 
lowerLimit = (numSamples/2) + (numSamples/100);
upperLimit = (numSamples/2) + (numSamples/30);

figure(1);
subplot(2,1,1)
plot(timeVec,sample);
xlim([0 3.5]);
title('Original sample');
xlabel('Time (seconds)');
ylabel('Amplitude');
subplot(2,1,2)
plot(timeVec2,sampleRateChanged);
xlim([0 3.5]);
title('Sample rate changed');
xlabel('Time (seconds)');
ylabel('Amplitude');

figure(2);
subplot(2,1,1)
plot(sample);
xlim([0 160416]);
title('Original sample');
xlabel('Samples');
ylabel('Amplitude');
subplot(2,1,2)
plot(sampleWithSilence);
xlim([0 160416]);
title('Sample with silence');
xlabel('Samples');
ylabel('Amplitude');