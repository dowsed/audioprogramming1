//
//  main.cpp
//  assignment2
//
//  Created by Pete Dowsett on 29/12/2019.
//  Copyright © 2019 Pete Dowsett. All rights reserved.
//

#include <iostream>
#include "WavHeader.h"

int main(int argc, char* argv[])
{
    //creates an object of type wav_hdr named wavHeader
    wav_hdr wavHeader;
    //creates two integer variables, headerSize which contains the size of wav_hdr (44 bytes) and fileLength which is initilised to 0.
    int headerSize = sizeof(wav_hdr), filelength = 0;
    //creates character pointer variable called filePath this is declared const so it cannot be changed
    const char* filePath;
    
    //created string variable named input
    string input;
    // argc looks for the number of arguments passed in when executing the program on the command line. By default this will be one (which is the command for executing the program itself). If that is the case it will execute the if condition which will prompt the user to enter the wave file path.
    if (argc <= 1)
    {
        cout << "Input wave file name: ";
        cin >> input;
        cin.get();
        filePath = input.c_str();
    }
    // if another argument is included in the command line when executing the program, that string will specify the filepath for the wave file that the user wants to read. argv is an array of char pointers (i.e. a c-style string) so the first string after the program execution will be read argv[1].
    else
    {
        filePath = argv[1];
        cout << "Input wave file name: " << filePath << endl;
    }
    // creating a FILE pointer object called wavFile and opening it using the fopen function. The fopen function requires two arguments the path of the file to be opened and the access mode, in the case of "r" this is read mode. The FILE struct and fopen function can be found in 'stdio.h' header file.
    FILE* wavFile = fopen(filePath, "r");
    
    // if the wavFile can't be read the pointer will not be pointing to anything. By checking to see if the pointer is null allows you to display an error message that the file was unable to be opened.
    if (wavFile == nullptr)
    {
        fprintf(stderr, "Unable to open wave file: %s\n", filePath);
        return 1;
    }

    //Read the header
    
    // creates a variable of type size_t called bytesRead. The size_t data type reprents the size of an object in bytes. The value stored in bytesRead is the return value of the fread (file read) function call. This returns the number of elements read by the function.
    
    // File read has the signature of fread(void * ptr, size_t size, size_t count, FILE * stream). it reads an array of count elements, each one with the size of size bytes, from the stream and stores that in a memory block ptr.
    
    // Therefore the fread call reads an array of headerSize elements, with the size of 1 and takes the data from the stream wavFile and stores it in wavHeader. Put simply the function is extracting the meta data in the wavFile and storing it in the wavHeader object.
    
    size_t bytesRead = fread(&wavHeader, 1, headerSize, wavFile);
    //prints to the console the number of bytesRead in the header file.
    cout << "Header Read " << bytesRead << " bytes." << endl;
    //provided at least one byte has been read, the values stored in wavHeader are stored in new variables bytesPerSample (uint16_t - 16 bit integer) and numSamples (uint64_t - 64 bit integer).
    
    /*/////////////////////////////////////ADDITIONAL CODE BRIEF 3.1///////////////////////////////////////*/
    //CODE TO CONVERT INPUT WAVE FILE TO 96KHz.
    //Create new header object which is identical to the original header
    wav_hdr wavHeader2 = wavHeader;
    //change the sample rate variable on the new header to 96KHz
    wavHeader2.SamplesPerSec = 96000;
    //create FILE pointer object for sample rate changed wav file
    FILE* wavFile2;
    //create a sample rate changed file and set int "write binary" mode
    wavFile2 = fopen("sampleRateChanged.wav", "wb");
    //error check to see if file has been created properly, if the pointer is null the file hasn't been created
    if (wavFile2 == nullptr)
    {
        cout << "Cannot create file\n";
        return 1;
    }
    //write updated header meta data to new file
    fwrite(&wavHeader2, headerSize, 1, wavFile2);
    //set the file pointer to the start of the data chunk ( i.e. after the header file)
    fseek(wavFile,44,SEEK_SET); //offsetting 44 bytes from the beginning of the wave file pointed to by wavFile
    //create buffer array of type uint16_t
    uint16_t buffer[wavHeader.ChunkSize];
    //read from wavFile into buffer
    fread(&buffer, wavHeader.ChunkSize, 1, wavFile);
    //copy the audio data to the new file from the buffer
    fwrite(&buffer, wavHeader.ChunkSize, 1, wavFile2);
    //close the new file
    fclose(wavFile2);
    
    //setting the FILE pointer back to the start of the data
    fseek(wavFile,44,SEEK_SET); //offsetting 44 bytes from the beginning of the wave file pointed to by wavFile
    /*/////////////////////////////////////END OF ADDITIONAL CODE/////////////////////////////////////////*/
    
    if (bytesRead > 0)
    {
        //Read the data
        uint16_t bytesPerSample = wavHeader.bitsPerSample / 8; // Calculates number of bytes per sample
        uint64_t numSamples = wavHeader.ChunkSize / bytesPerSample; //Calculates how many samples are in the wav file by dividing the data chunk size by the number of bytes per sample
        static const uint16_t BUFFER_SIZE = 4096; //create const variable for a set buffer size of 4096, this is static so it has the lifetime of the entire program
        int16_t* buffer = new int16_t [BUFFER_SIZE]; //creates a new pointer called buffer of 16 bits . This is pointing to an array of int16_t with BUFFER_SIZE number of elements, this is created dynamically (on the heap).
        
        /*/////////////////////////////////////ADDITIONAL CODE BRIEF 3.3///////////////////////////////////////*/
        //initialising additional variables
        //create FILE pointer object for sample rate changed wav file
        FILE* wavFile3;
        //create a sample rate changed file and set int "write binary" mode
        wavFile3 = fopen("sampleWithSilence.wav", "wb");
        //write header meta data to new file
        fwrite(&wavHeader, headerSize, 1, wavFile3);
        //error check to see if file has been created properly, if the pointer is null the file hasn't been created
        if (wavFile3 == nullptr)
        {
            cout << "Cannot create file\n";
            return 1;
        }
        //three temporary variables for checking conditions in main loop
        uint64_t lowerLimit = (numSamples/2) + (numSamples/100);
        uint64_t upperLimit = (numSamples/2) + (numSamples/30);
        int currentSample = 1;
        /*/////////////////////////////////////END OF ADDITIONAL CODE/////////////////////////////////////////*/
        
        //This is the main processing loop for the wave data chunk, again it uses the fread function to take the data from wavFile and store it in the buffer in blocks with a size of bytesPerSample and there are BUFFER_SIZE of them.
        while ((bytesRead = fread(buffer, bytesPerSample, BUFFER_SIZE, wavFile)) > 0)
        {
            /*/////////////////////////////////////ADDITIONAL CODE BRIEF 3.3///////////////////////////////////////*/
            //create a pause in the audio file between (N/2 + N/100) see variable lowerLimit and (N/2 + N/30) see variable upperLimit, where N is the sample index
            
            //loops through all the bytes read in each buffer
            for (int i = 0; i < bytesRead; ++i)
            {
                //checks if it is between the limits specified
                if ( (currentSample >= lowerLimit) && (currentSample <= upperLimit) )
                {
                    //if true the data in that sample of the buffer is replaced with 0.
                    buffer[static_cast<uint16_t>(i)] = 0; //for safety the i variable (which is an int) is cast to the uint16_t expected by the array
                }
                ++currentSample;
            }
            //Writes the buffer to wavFile3 with a size of bytesPerSamples and bytesRead of them. Note the call to the write function here does not use the BUFFER_SIZE variable as the buffer is not guaranteed to be filled if you reach the end of the file.
            fwrite(buffer, bytesPerSample, bytesRead, wavFile3);
            /*/////////////////////////////////////END OF ADDITIONAL CODE/////////////////////////////////////////*/
            
            //console out to confirm each buffer has been read and the amount of data in the buffer. All should be 4096 (ie BUFFER_SIZE) until the last buffer block of the file which is likely to be less than a full buffer size.
            cout << "Read " << bytesRead << " bytes." << endl;
        }
        //close the new file
        fclose(wavFile3);
        
        // deletes the array associated with buffer and removes that memory from the heap
        delete [] buffer;
        // removes the pointer to the memory address stored by buffer
        buffer = nullptr;
        //calculates the filesize of the wave file by calling the getFileSize function found at the bottom of this cpp file. It passes the argument of a pointer to a file, in this case the wavFile.
        filelength = getFileSize(wavFile);

        //prints out to the console different attributes from the wavHeader object. This gives the user a summary of the wave files header information.
        
        cout << "File is :" << filelength << " bytes." << endl;
        cout << "RIFF header :" << wavHeader.RIFF[0] << wavHeader.RIFF[1] << wavHeader.RIFF[2] << wavHeader.RIFF[3] << endl;
        cout << "WAVE header :" << wavHeader.WAVE[0] << wavHeader.WAVE[1] << wavHeader.WAVE[2] << wavHeader.WAVE[3] << endl;
        cout << "FMT :" << wavHeader.fmt[0] << wavHeader.fmt[1] << wavHeader.fmt[2] << wavHeader.fmt[3] << endl;
        cout << "Data size :" << wavHeader.ChunkSize << endl;

        // Display the sampling Rate from the header
        cout << "Sampling Rate :" << wavHeader.SamplesPerSec << endl;
        cout << "Number of bits used :" << wavHeader.bitsPerSample << endl;
        cout << "Number of channels :" << wavHeader.NumOfChan << endl;
        cout << "Number of bytes per second :" << wavHeader.bytesPerSec << endl;
        cout << "Data length :" << wavHeader.Subchunk2Size << endl;
        cout << "Audio Format :" << wavHeader.AudioFormat << endl;
        // Audio format 1=PCM,6=mulaw,7=alaw, 257=IBM Mu-Law, 258=IBM A-Law, 259=ADPCM

        cout << "Block align :" << wavHeader.blockAlign << endl;
        cout << "Data string :" << wavHeader.Subchunk2ID[0] << wavHeader.Subchunk2ID[1] << wavHeader.Subchunk2ID[2] << wavHeader.Subchunk2ID[3] << endl;
    }
    fclose(wavFile);
    return 0;
}

// find the file size
int getFileSize(FILE* inFile)
{
    int fileSize = 0;
    fseek(inFile, 0, SEEK_END); //sets to the end position of the FILE which is pointed to by inFile

    fileSize = static_cast<int>(ftell(inFile)); //finds the size of the file with ftell() because the FILE pointer is positioned at the end of the file and the result is cast to an integer and stored in fileSize;

    fseek(inFile, 0, SEEK_SET); //sets the inFile pointer back to the start position of the file
    return fileSize;
}
